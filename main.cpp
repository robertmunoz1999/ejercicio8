// vector::push_back
#include <iostream>
#include <vector>

int main ()
{
  std::vector<int> myvector;
  int myint;

  std::cout << "Please enter some integers (enter 0 to end):" << std::endl;
  std::cin >> myint;
  std::cout << myint;
  for(int i=0; i<myint; i++ ) {
    int val = rand() % 100;
    myvector.push_back (val);
    
  }

  std::cout << "myvector stores " << int(myvector.size()) << " numbers.\n";
  for(auto v : myvector) {
    std::cout << v << std::endl;
  }
  return 0;
}